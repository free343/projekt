import template from './app.html';

const appComponent = () => {
    return {
        templateUrl: template,
        restrict: 'E'
    };
};

export default appComponent;
