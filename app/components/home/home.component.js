import template from './home.html';
import controller from './home.controller';

const homeComponent = {
    restrict: 'E',
    scope: {},
    templateUrl: template,
    controller: controller,
    controllerAs: 'vm',
    bindToController: true
};

export default homeComponent;
