import angular from 'angular';
import Home from './home/home';
import Task from './task/task';

export default angular.module('app.components', [
    Home.name,
    Task.name
]);
