import angular from 'angular';
import uiRouter from 'angular-ui-router';
import taskComponent from './task.component';

const taskModule = angular.module('task', [
    uiRouter
])

.config(($stateProvider, $urlRouterProvider) => {
    $urlRouterProvider.otherwise('/');

    $stateProvider
    .state('task', {
        url: '/task',
        template: '<task></task>'
    });
})

.component('task', taskComponent);

export default taskModule;
