import template from './task.html';
import controller from './task.controller';

const taskComponent = {
    restrict: 'E',
    scope: {},
    templateUrl: template,
    controller: controller,
    controllerAs: 'vm',
    bindToController: true
};

export default taskComponent;
