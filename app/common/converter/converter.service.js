class ConverterService {

    convertBase(value, fromBase, toBase) {
        const range = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ+/'.split('');
        const fromRange = range.slice(0, fromBase);
        const toRange = range.slice(0, toBase);
        let decValue = value.split('').reverse().reduce((carry, digit, index) => {
            let carryReturn = carry;
            if (fromRange.indexOf(digit) === -1) { throw new Error('Invalid digit `' + digit + '` for base ' + fromBase + '.'); }
            carryReturn += fromRange.indexOf(digit) * (Math.pow(fromBase, index));
            return carryReturn;
        }, 0);

        let newValue = '';
        while (decValue > 0) {
            newValue = toRange[decValue % toBase] + newValue;
            decValue = (decValue - (decValue % toBase)) / toBase;
        }
        newValue = newValue || '0';
        return newValue;
    }

}

export default ConverterService;
