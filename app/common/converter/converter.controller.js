class ConverterController {
    constructor(converterService) {
        this.converterService = converterService;
        this.value = '';
        this.fromBase = '';
        this.toBase = '';
        this.resoult = '';
    }

    convert() {
        try {
            return this.converterService.convertBase(this.value, this.fromBase, this.toBase);
        } catch (err) {
            /*eslint-disable */
            alert(err.message);//toDo component to menage errors.
            /*eslint-enable */
        }
    }

    show() {
        this.resoult = this.convert();
    }
}

export default ConverterController;
