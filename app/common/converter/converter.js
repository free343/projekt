import angular from 'angular';
import uiRouter from 'angular-ui-router';
import converterComponent from './converter.component';
import converterService from './converter.service';

const converterModule = angular.module('converter', [
    uiRouter
])

.directive('converter', converterComponent)
.service('converterService', converterService);

export default converterModule;
