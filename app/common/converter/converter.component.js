import './converter.scss';
import template from './converter.html';
import controller from './converter.controller';

const converterComponent = function () {
    return {
        restrict: 'E',
        scope: {},
        templateUrl: template,
        controller: controller,
        controllerAs: 'vm',
        bindToController: true
    };
};

export default converterComponent;
