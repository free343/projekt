import angular from 'angular';
import Navbar from './navbar/navbar';
import Converter from './converter/converter';

export default angular.module('app.common', [
    Navbar.name,
    Converter.name
]);
