import './navbar.scss';
import template from './navbar.html';
import controller from './navbar.controller';

const navbarComponent = function () {
    return {
        restrict: 'E',
        scope: {},
        templateUrl: template,
        controller: controller,
        controllerAs: 'vm',
        bindToController: true
    };
};

export default navbarComponent;
